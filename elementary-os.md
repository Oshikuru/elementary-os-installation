# elementaryOS installation instructions

## Contents <!-- omit in toc -->

- [OS Dark Theme](#os-dark-theme)
- [OSX Theme](#osx-theme)
- [Google Chrome](#google-chrome)
- [Flathub Apps](#flathub-apps)
    - [Discord](#discord)
    - [Slack](#slack)
- [Mailspring](#mailspring)
- [Calendar](#calendar)
- [Development](#development)
    - [GCC](#gcc)
    - [Clang](#clang)
    - [CUDA Toolkit](#cuda-toolkit)
    - [cuDNN](#cudnn)
    - [Vulkan SDK](#vulkan-sdk)
    - [Python](#python)
    - [Jetbrains Toolbox App](#jetbrains-toolbox-app)
    - [Git](#git)
    - [Docker](#docker)

## OS Dark Theme

```
sudo apt install software-properties-common
sudo add-apt-repository ppa:philip.scott/elementary-tweaks
sudo apt install elementary-tweaks
```

System Setting -> Tweaks -> Prefer dark variant

## OSX Theme

Prerequisite: install [git](#git).

```
git clone https://github.com/ipproductions/eOS-X.git
sudo mv ./theme/eOS\ X/ /usr/share/themes
rm -r eOS-X
```

## Google Chrome

```
wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
echo 'deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main' | sudo tee /etc/apt/sources.list.d/google-chrome.list
sudo apt-get update 
sudo apt-get install google-chrome-stable
```

## Flathub Apps

Download app from [flathub](https://flathub.org/home).

### Discord

[Discord app](https://flathub.org/apps/details/com.discordapp.Discord)

### Slack

[Slack app](https://flathub.org/apps/details/com.slack.Slack)

## Mailspring

```
sudo apt update
sudo apt install snapd
sudo snap install mailspring
```

Reboot.

## Calendar

Install Calendar GNOME from AppCenter.

```
sudo apt install gnome-online-accounts
sudo apt install evolution evolution-ews
XDG_CURRENT_DESKTOP=GNOME gnome-control-center
```

Setup connected accounts.


## Development

Tools for python and c++ development.

### GCC

C/C++ compiler. Current default installed gcc-7.
```
sudo add-apt-repository ppa:ubuntu-toolchain-r/test
sudo apt-get update
```

Install any desired version (current stable is 9)
```
sudo apt-get install gcc-9
```

Optional: set it as default (be aware latest CUDA toolkit supports gcc-8 as maximum version)
```
sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-9 60 --slave /usr/bin/g++ g++ /usr/bin/g++-9
```

### Clang

C/C++ compiler.

Go to [llvm website](https://apt.llvm.org/) and under ubuntu find correct version (current Bionic (18.04)) and copy text.
```
sudo io.elementary.code /etc/apt/sources.list
```
Paste the text at the end and save the file.

Install desired version (current stable 10)
```
wget -O - https://apt.llvm.org/llvm-snapshot.gpg.key|sudo apt-key add -
sudo apt-get install clang-10 lldb-10 lld-10
sudo apt-get install libllvm-10-ocaml-dev libllvm10 llvm-10 llvm-10-dev llvm-10-doc llvm-10-examples llvm-10-runtime
sudo apt-get install clang-10 clang-tools-10 clang-10-doc libclang-common-10-dev libclang-10-dev libclang1-10 clang-format-10 python-clang-10 clangd-10
sudo apt-get install libc++-10-dev libc++abi-10-dev
sudo apt-get install libomp-10-dev
```

Optional: set it as default.
```
sudo update-alternatives --install /usr/bin/clang clang /usr/bin/clang-10 60 --slave /usr/bin/clang++ clang++ /usr/bin/clang++-10
```

### CUDA Toolkit

```
wget https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64/cuda-ubuntu1804.pin
sudo mv cuda-ubuntu1804.pin /etc/apt/preferences.d/cuda-repository-pin-600
sudo apt-key adv --fetch-keys https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64/7fa2af80.pub
sudo add-apt-repository "deb http://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64/ /"
sudo apt-get update
```

See available version with:
```
apt policy cuda
```

Install version supported by tensorflow (current 10.1)

```
sudo apt-get install cuda
```

Reboot.

### cuDNN

[Download](https://developer.nvidia.com/rdp/cudnn-download) cuDNN for the CUDA desired version (current 10.1). Download runtime and developer library (sign in will be required).

```
sudo dpkg -i libcudnn7_7.6.5.32-1+cuda10.2_amd64.deb
sudo dpkg -i libcudnn7-dev_7.6.5.32-1+cuda10.2_amd64.deb
```

### Vulkan SDK

Go to [Vulkan SDK website](https://vulkan.lunarg.com/sdk/home#linux). Find latest SDK version under Ubuntu packages. Under Ubuntu 18.04 (Bionic Beaver) copy and paste in terminal (for current latest):
```
wget -qO - http://packages.lunarg.com/lunarg-signing-key-pub.asc | sudo apt-key add -
sudo wget -qO /etc/apt/sources.list.d/lunarg-vulkan-1.2.135-bionic.list http://packages.lunarg.com/vulkan/1.2.135/lunarg-vulkan-1.2.135-bionic.list
sudo apt update
sudo apt install vulkan-sdk
```

### Python

Install pip3:
```
sudo apt install python3-pip
```

Install any other packages through virtual environments.

### Jetbrains Toolbox App

Download [toolbox](https://www.jetbrains.com/toolbox-app/). Extract it and run it. Sign in and install CLion and PyCharm.
Configure CLion path variables for CUDA.
Configure python virtual environments in PyCharm.

### Git

```
sudo apt install git
```

### Docker

```
sudo apt-get update

sudo apt-get install apt-transport-https ca-certificates curl gnupg-agent software-properties-common
    
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"

sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io
```